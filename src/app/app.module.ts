import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import {RouterModule, Routes} from "@angular/router";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { ContactComponent } from './components/contact/contact.component';
import { TeamComponent } from './components/team/team.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { ReferencesComponent } from './components/references/references.component';
import { AboutComponent } from './components/about/about.component';
import { BannerComponent } from './components/home/banner/banner.component';
import { AboutusComponent } from './components/home/aboutus/aboutus.component';
import { FooterComponent } from './components/footer/footer.component';

const routes:Routes = [
  {
    path:'',
    component:HomeComponent,
  },
  {
    path:'about',
    component:AboutComponent,
    data: {animation: 'FilterPage'}

  },
  {
    path:'contact',
    component:ContactComponent,
  },
  {
    path:'projects',
    component:ProjectsComponent,
  },
  {
    path:'references',
    component:ReferencesComponent,
  },

];



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ContactComponent,
    TeamComponent,
    ProjectsComponent,
    ReferencesComponent,
    AboutComponent,
    BannerComponent,
    AboutusComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
}),
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  exports:[
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
